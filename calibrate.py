import click
import cv2
import logging
import numpy as np

from skimage.io import imread
from pathlib import Path

import objects


logger = logging.getLogger(__name__)


CHECKERBOARD = (19, 12)


CRITERIA = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
            30, 0.1)


class CalibrationError(RuntimeError):
    pass


class TupleIntType(click.ParamType):
    name = '(int, int)'

    def convert(self, value, param, ctx):
        """
        Tries to convert the given value into a tuple of integers
        with length 2.
        """
        try:
            v1, v2 = map(int, value.strip('()').split(','))
        except (IndexError, ValueError):
            self.fail(
                '{} is not a valid tuple of integer with size 2'.format(value))
        return (v1, v2)


def load_images(search_path):
    """
    Loads .mpo files from the given path and
    extracts the images of the left and right camera.

    Returns
        Generator of `StereoImage`
    """
    path = Path(search_path)
    for img_path in path.glob('**/*.MPO'):
        img = imread(img_path)
        if len(img.shape) != 4:
            logger.error(
                '{} does not contain multiple images'.format(img_path)
            )
            continue

        yield objects.StereoImage(
            objects.Image(img[1], img_path.name, 'left'),
            objects.Image(img[0], img_path.name, 'right'),
            img_path.name,
        )


def _find_chessboard_corners(imgc, pattern_size, draw=False):
    """
    Finds checkerboard on the given image and returns the coordinates of each
    crossing as list.
    """
    img = imgc.grayscale()
    found, corners = cv2.findChessboardCorners(img.data, pattern_size)

    if found:
        cv2.cornerSubPix(img.data, corners, (5, 5), (-1, -1),
                         CRITERIA)
    else:
        raise ValueError(
            'Could not find chessboard on {}.'.format(img.name)
        )

    if draw:
        # Draw and display the corners
        cv2.drawChessboardCorners(imgc.data, CHECKERBOARD, corners, True)
        imgc.show()
        input()
    return corners


def _get_intrinsic_parameters(img_points, obj_points, img_size):
    """
    Calcualtes the intrinsic camera parameters for the given
    checkerboard points.

    Returns
        objects.CameraParameters
    """
    # calculate camera distortion
    ret, camera_matrix, dist_coefs, _, _ = cv2.calibrateCamera(
        obj_points, img_points, img_size, None, None)

    if not ret:
        raise CalibrationError(
            'Could not calculate intrinsic parameters.'
        )

    return objects.CameraParameters(camera_matrix, dist_coefs)


def _get_extrinsic_parameters(initial_camera_left, initial_camera_right,
                              obj_points,
                              img_points_left, img_points_right,
                              img_size):
    """
    Calculates the extrinsic parameters, based on the calibrated cameras
    and the given image points.

    Returns
        objects.StereoCamera
    """
    (ret,
     camera_matrix_left, dist_coefs_left,
     camera_matrix_right, dist_coefs_right,
     rotation_matix, translation_matrix,
     essential_matrix, fundamental_matrix) = cv2.stereoCalibrate(
        obj_points, img_points_left, img_points_right,
        initial_camera_left.matrix, initial_camera_left.distortion,
        initial_camera_right.matrix, initial_camera_right.distortion,
        img_size,
        flags=(
            cv2.CALIB_USE_INTRINSIC_GUESS +  # optimize intrinsic params
            cv2.CALIB_FIX_ASPECT_RATIO +  # optimize f_y
            cv2.CALIB_FIX_FOCAL_LENGTH +  # optimize f_x
            cv2.CALIB_ZERO_TANGENT_DIST +
            cv2.CALIB_FIX_K1 +
            cv2.CALIB_FIX_K2 +
            cv2.CALIB_FIX_K3
        ),
        criteria=CRITERIA)

    if not ret:
        raise CalibrationError(
            'Could not calculate extrinsic parameters.'
        )

    left_camera = objects.CameraParameters(
        camera_matrix_left, dist_coefs_left)
    right_camera = objects.CameraParameters(
        camera_matrix_right, dist_coefs_right)

    return objects.StereoCamera(
        left=left_camera,
        right=right_camera,
        rotation_matrix=rotation_matix,
        translation_matrix=translation_matrix,
        essential_matrix=essential_matrix,
        fundamental_matrix=fundamental_matrix,
    )


def calibrate(images, pattern_size):
    """
    Calculates the intrinsics of each camera and the extrinsics of the stereo
    camera.

    Returns
        objects.StereoCamera
    """
    img_points_left = []  # 2d points in image plane
    img_points_right = []  # 2d points in image plane

    img = None
    for img in images:
        print('Detecting checkerboard on `{}`'.format(img.name))
        try:
            corners_left = _find_chessboard_corners(img.left, pattern_size)
            corners_right = _find_chessboard_corners(img.right, pattern_size)
        except ValueError:
            logger.warning(
                'Could not find checkerboard on {} or {}.'.format(
                    img.left.name,
                    img.right.name,
                )
            )
            continue
        else:
            img_points_left.append(corners_left)
            img_points_right.append(corners_right)

    if img is None:
        raise CalibrationError(
            'Could not load images.'
        )

    # use size of last image
    img_size = (img.width, img.height)

    # prepare object points of one checkerboard,
    # like (0,0,0), (1,0,0), (2,0,0) ..,(6,5,0)
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)

    # we need object points for each image
    # it represents a 3d point in real world space
    obj_points = [pattern_points for _ in range(len(img_points_left))]

    # calibrate each camera
    print('Calculate intrinsics')
    camera_left = _get_intrinsic_parameters(
        img_points_left, obj_points, img_size)
    camera_right = _get_intrinsic_parameters(
        img_points_right, obj_points, img_size)

    # calibrate the stereo camera
    print('Calculate extrinsics')
    return _get_extrinsic_parameters(
        camera_left, camera_right,
        obj_points,
        img_points_left, img_points_right,
        img_size)


@click.option('--checkerboard', default=str(CHECKERBOARD), type=TupleIntType())
@click.argument('images_path', type=click.Path(exists=True, file_okay=False,
                                               dir_okay=True))
@click.argument('output_file', type=click.File('w'))
@click.command()
def run(checkerboard, images_path, output_file):
    images = load_images(images_path)
    parameters = calibrate(images, checkerboard)
    parameters.save(output_file)
    print('Stored parameters to `{}`'.format(output_file.name))


if __name__ == '__main__':
    run()
