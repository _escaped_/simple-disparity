import click
import cv2
import logging

from skimage.io import imread

import objects


logger = logging.getLogger(__name__)


def _rectify_image(image, camera):
    """
    Rectifies a image pair using the given camera parameters.

    Returns
        objects.StereoImage
    """
    img_size = (image.width, image.height)

    (rotation_matrix_l, rotation_matrix_r,
     projection_matrix_l, projection_matrix_r, _, _, _) = cv2.stereoRectify(
        camera.left.matrix, camera.left.distortion,
        camera.right.matrix, camera.right.distortion,
        img_size,
        camera.rotation_matrix,
        camera.translation_matrix,
        flags=0,
        alpha=0,  # project only valid pixels (after transform)
    )

    map_x_left, map_y_left = cv2.initUndistortRectifyMap(
        camera.left.matrix,
        camera.left.distortion,
        rotation_matrix_l,
        # camera_params.left.new_camera_matrix,  # or use projection matrix
        projection_matrix_l,
        img_size,
        m1type=cv2.CV_32FC1,
    )
    map_x_right, map_y_right = cv2.initUndistortRectifyMap(
        camera.right.matrix,
        camera.right.distortion,
        rotation_matrix_r,
        projection_matrix_r,
        img_size,
        m1type=cv2.CV_32FC1,
    )

    image_rect_l = cv2.remap(
        image.left.data, map_x_left, map_y_left,
        interpolation=cv2.INTER_CUBIC
    )
    image_rect_r = cv2.remap(
        image.right.data, map_x_right, map_y_right,
        interpolation=cv2.INTER_CUBIC
    )

    return objects.StereoImage(
        objects.Image(image_rect_l, image.left.name, 'left, rectified'),
        objects.Image(image_rect_r, image.right.name, 'right, rectified'),
        image.left.name,
    )


def _get_disparity(rect_image, camera, normalize=True):
    """
    Calculates a disparity map and convert the output to a grayscale image.

    Returns
        objects.Image
    """
    # use grayscale image to calucalte disparity map
    rect_image = objects.StereoImage(
        rect_image.left.grayscale(),
        rect_image.right.grayscale(),
        rect_image.left.name,
    )

    # compute disparity map
    window_size = 10
    color_channels = 3 if len(rect_image.left.data.shape) == 3 else 1
    min_disp = 0
    num_disp = 112 - min_disp
    stereo = cv2.StereoSGBM_create(
        minDisparity=min_disp,
        numDisparities=num_disp,
        blockSize=window_size,
        P1=8 * color_channels * window_size ** 2,
        P2=32 * color_channels * window_size ** 2,
        disp12MaxDiff=1,
        uniquenessRatio=10,
        speckleWindowSize=100,
        speckleRange=32,
    )

    # calc disparity map
    disp = stereo.compute(
        rect_image.left.data,
        rect_image.right.data,
    )

    # to visualize the result as a grayscale image,
    # we need to normalize all values

    # all values <0 couldn't be computed, replace them by zero
    disp[disp < 0] = 0

    # normalize values to be between 0 and 255
    if disp.max() and normalize:
        norm_coeff = 255 / disp.max()
        disp = disp * norm_coeff / 255

    return objects.Image(disp, rect_image.name, 'disparity')


def load_image(img_path):
    img = imread(img_path)

    if len(img.shape) != 4:
        logger.error(
            '{} does not contain multiple images'.format(img_path)
        )

    return objects.StereoImage(
        objects.Image(img[1], img_path.name, 'left'),
        objects.Image(img[0], img_path.name, 'right'),
        img_path.name,
    )


@click.argument('camera_params', type=click.File('r'))
@click.argument('mpo_file', type=click.File('rb', lazy=True))
@click.command()
def run(camera_params, mpo_file):
    camera = objects.StereoCamera.load(camera_params)
    image = load_image(mpo_file)
    rect_image = _rectify_image(image, camera)
    disp = _get_disparity(rect_image, camera)
    rect_image.show()
    disp.show()


if __name__ == '__main__':
    run()
