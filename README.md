
## Requirements

* Python 3.x
* imagemagic

## Install


        python -m venv _venv
        _venv/bin/pip install -U pip
        _venv/bin/pip install -r requirements.txt 


## calibrate

The calibration is done using a Chessboard pattern. Simply run
`_venv/bin/python calibrate.py camera.json <path to dir with chessboard *.MPO files>`.
This calculates the intrinsic and extrinsic camera parameters and stores them in
a `.json` file.

For further options see `_venv/bin/python calibrate.py --help`.


## generate disparity map
To display a disparity map from `.MPO` file, simply run the following command
with the generated `.json` from the calibration step.

        _venv/bin/python disparity.py <path to .MPO file> camera.json
