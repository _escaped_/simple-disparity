import attr
import cv2
import json
import numpy as np

from pathlib import Path
from scipy.misc import toimage
from skimage.io import imread


@attr.s
class Image(object):
    data = attr.ib()
    name = attr.ib(validator=attr.validators.instance_of(str))
    scope = attr.ib(validator=attr.validators.instance_of(str),
                    default='')

    @property
    def height(self):
        return self.data.shape[0]

    @property
    def width(self):
        return self.data.shape[1]

    @property
    def shape(self):
        return self.data.shape

    @property
    def full_name(self):
        return '{} ({})'.format(self.name, self.scope)

    def grayscale(self):
        return Image(
            cv2.cvtColor(self.data, cv2.COLOR_BGR2GRAY),
            self.name,
            ', '.join([self.scope, 'grayscale'])
        )

    def show(self):
        toimage(self.data).show()

    @staticmethod
    def load(self, path):
        data = imread(path)
        name = Path(path).name
        return Image(data, name)


@attr.s
class StereoImage(object):
    left = attr.ib(validator=attr.validators.instance_of(Image))
    right = attr.ib(validator=attr.validators.instance_of(Image))
    name = attr.ib(validator=attr.validators.instance_of(str))

    @property
    def width(self):
        return self.left.width

    @property
    def height(self):
        return self.left.height

    def concat(self):
        img_data = np.concatenate((self.left.data, self.right.data), axis=1)
        img_name = self.left.name
        return Image(img_data, img_name, 'HSBS')

    def show(self):
        self.concat().show()


@attr.s
class CameraParameters(object):
    matrix = attr.ib()
    distortion = attr.ib()

    def to_json(self):
        return {
            'matrix': self.matrix.tolist(),
            'distortion': self.distortion.tolist(),
        }

    @staticmethod
    def from_json(json_str):
        data = json.loads(json_str)
        return CameraParameters(
            np.array(data['matrix']),
            np.array(data['distortion']),
        )


@attr.s
class StereoCamera(object):
    left = attr.ib(validator=attr.validators.instance_of(CameraParameters))
    right = attr.ib(validator=attr.validators.instance_of(CameraParameters))

    rotation_matrix = attr.ib()
    translation_matrix = attr.ib()
    essential_matrix = attr.ib()
    fundamental_matrix = attr.ib()

    def to_json(self):
        return {
            'left': self.left.to_json(),
            'right': self.right.to_json(),
            'rotation_matrix': self.rotation_matrix.tolist(),
            'translation_matrix': self.translation_matrix.tolist(),
            'essential_matrix': self.essential_matrix.tolist(),
            'fundamental_matrix': self.fundamental_matrix.tolist(),
        }

    @staticmethod
    def from_json(json_str):
        data = json.loads(json_str)
        return StereoCamera(
            CameraParameters.from_json(json.dumps(data['left'])),
            CameraParameters.from_json(json.dumps(data['right'])),
            np.array(data['rotation_matrix']),
            np.array(data['translation_matrix']),
            np.array(data['essential_matrix']),
            np.array(data['fundamental_matrix']),
        )

    @staticmethod
    def load(fp):
        return StereoCamera.from_json(fp.read())

    def save(self, fp):
        fp.write(json.dumps(self.to_json()))
